const api_key = '81cf50a9f2d0b5efbc412a1c77b75064';

async function result(event){
    if (event.keyCode === 13) {
        event.preventDefault();
        const ville = document.getElementById("ville")
        const content = document.getElementById("content")
        const data = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${ville.value}&appid=${api_key}&units=metric`)
        const res =  await data.json();
        console.log(res);
        content.innerHTML = ` <div class="card-top text-center">
        <div class="nom-ville my-3">
          <p>${res.name}</p>
          <span>${Math.round(res.main.temp)}&deg;C</span>
        </div>
        <img src="img/temps-nuageux.jpeg" class="card-image-top time" alt="">
      </div>
      <div class="card-body">
        <div class="card-mid row">
          <div class="col text-center temp">
            <p>Max</p>
            <p class="max">${Math.round(res.main.temp_max)}&deg;C</p>
          </div>
          <div class="col text-center">
            <p class="condition">${res.weather[0].main}</p>
          </div>
          <div class="col text-center condition-temp">
            <p>Min</p>
            <p class="min">${Math.round(res.main.temp_min)}&deg;C</p>
          </div>
        </div>
        <div class="icon-container card shadow mx-auto">
        <img src="http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png"/>
        </div>
        <div class="col text-center feel-like">
          <p>Feels like</p>
          <span>${Math.round(res.main.feels_like)}&deg;C</span>
        </div>
        <!-- <div class="icon-container card shadow mx-auto">
          <img src="img/pluvieux.png" alt="">
        </div> -->
        <div class="card-bottom px-5 py-4 row">
          <div class="col text-center">
            <p>${res.main.humidity}%</p>
            <span>Humidité</span>
          </div>
          <div class="col text-center">
            <p>${Math.round(res.wind.speed*3)}km/h</p>
            <span>vent</span>
          </div>
        </div>
      </div>`
        

    }
}





async function auto(e){
    const data = await fetch("https://api-adresse.data.gouv.fr/search/?q=" + e +"&autocomplete=0")
    const res = await data.json()
    // console.log(res)
    const results = document.getElementById('result')
    results.innerHTML = ""
if(e.length == 0){
    results.style.display = "none"
}

if(e.length > 3){
    results.style.display = "block"
    res.features.forEach(e => {
        results.innerHTML += `<p>${e.properties.city}</p>`
    })
}
  

}

const search = document.getElementById('ville')

search.addEventListener('keyup', result)
