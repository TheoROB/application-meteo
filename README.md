## Install

```shell
# Install dependencies
npm i
```

## Usage

# Start local server (uses sirv-cli)
npm run server


| Script | Description |
| --- | --- |
| `server` | Starts a local server (<http://localhost:3000>) for development |

